import time
import math

from sr.robot import *

R = Robot()

###########################################
Time_1cm = 0.101

Time_1dg =  0.0095

###########################################



#############################################################
#			Funktionen			    #
#############################################################

def drive(cm):
 if cm > 0:
  driveForward(cm * Time_1cm)
 else: 
  driveBackward(cm *Time_1cm * (-1))
  
  

def turn(dg):
 if dg > 0:
  turnRight(dg * Time_1dg)
 else: 
  turnLeft(dg *Time_1dg * (-1))
    

def driveForward(Sekunde):
 R.motors[0].m0.power = 25
 R.motors[0].m1.power = 25
 R.sleep(Sekunde)
 nodrive(1)
 
 

def driveBackward(Sekunde):
 R.motors[0].m0.power = -25
 R.motors[0].m1.power = -25
 R.sleep(Sekunde)
 nodrive(1)

def nodrive(Sekunde):
 R.motors[0].m0.power = 0
 R.motors[0].m1.power = 0
 R.sleep(Sekunde)

def turnLeft(Sekunde):
 R.motors[0].m0.power = 25
 R.motors[0].m1.power = -25
 R.sleep(Sekunde)
 nodrive(1)

def turnRight(Sekunde):
	R.motors[0].m0.power = -25
	R.motors[0].m1.power = 25
	R.sleep(Sekunde)
	nodrive(1)

def deleatTokenMarker(markers):
    ArenaMarkers = []
    if len(markers)==0:
        return ArenaMarkers
    for oneMarker in markers:
        if oneMarker.info.code == MARKER_ARENA:
            ArenaMarkers.append(oneMarker)
        return ArenaMarkers



def deleatArenaMarker(markers):
	tokenMarkers = []
	if len(markers) == 0:
		return tokenMarkers
	for oneMarker in markers:
		if oneMarker.info.marker_type == MARKER_TOKEN_SILVER or oneMarker.info.marker_type == MARKER_TOKEN_GOLD:
			tokenMarkers.append(oneMarker)
	return tokenMarkers

def findClosesedMarker(markers):
	if len(markers) == 0:
		return
	returnMarker = markers[0]
	for oneMarker in markers:
		if oneMarker.centre.polar.length < returnMarker.centre.polar.length:
			returnMarker = oneMarker
	return returnMarker

def findSameMarker(markers, markerNumber):
	if len(markers) == 0:
		return
	for oneMarker in markers:
		if oneMarker.info.code == markerNumber:
			return oneMarker
	return

def driveToToken():
	seenMarkers = R.see()
	print(seenMarkers)
	seenTokenMarkers = deleatArenaMarker(seenMarkers)
	closesedMarker = findClosesedMarker(seenTokenMarkers)
	print(closesedMarker)
	if closesedMarker == None:
		turn(-20)
		driveToToken()
	else:
		turnBy = -closesedMarker.centre.polar.rot_y
		driveBy = closesedMarker.centre.polar.length*100
		driveBy = driveBy
		theMarkerNumber = closesedMarker.info.code
		while driveBy > 40:
	 #print("lööp")
		 sideoffset = math.cos(math.radians(turnBy))*driveBy
		 ratio = 1.0-((sideoffset/driveBy)/4)
		 if turnBy > 0:
		  R.motors[0].m0.power = 25.0*ratio
		  R.motors[0].m1.power = 25.0
		 else:
		  R.motors[0].m0.power = 25.0
		  R.motors[0].m1.power = 25.0*ratio
		 seenMarkers = R.see()
		 seenTokenMarkers = deleatArenaMarker(seenMarkers)
		 markerFromLastImage = findSameMarker(seenTokenMarkers, theMarkerNumber)
		 turnBy = -markerFromLastImage.centre.polar.rot_y
		 driveBy = markerFromLastImage.centre.polar.length*100
		 #R.sleep(0.1)
		nodrive(1)


def fidgetspinner():
  R.motors[0].m0.power = -100
  R.motors[0].m1.power = 100
  R.sleep(20)


def closeArm():
    R.motors[1].m1.power = -25
    R.sleep(2)

def openArm():
    R.motors[1].m1.power = 15
    R.sleep(2)

def ArmDown():
    R.motors[1].m0.power = 15
    R.sleep(2)


def ArmUp():
    R.motors[1].m0.power = -10
    R.sleep(0.1)
    R.motors[1].m0.power = 0

def findmarker(Code1, Code2 , Code3 , Code4):
    markers = R.see()
    print (markers)
    for oneMarker in markers:
        if oneMarker.info.code == Code1 or oneMarker.info.code == Code2 or oneMarker.info.code == Code3 or oneMarker.info.code == Code4:
            return oneMarker

def turn1(code1 ,code2, code3 , code4):
    markers = R.see()
    foundmarker = findmarker(code1 ,code2, code3 , code4)
    print (foundmarker)
    if foundmarker == None:
      turn(1)
    else:
      turnby = foundmarker.centre.polar.rot_y
      turn(turnby*(-1))


def turn0():
    markers = R.see()
    print (markers)
    turnby = markers[0].centre.polar.rot_y
    turn(turnby*(-1))

#########################################################
#			code 				#
#########################################################




R.sleep(2)
turn(-35)
drive(-50)
drive(57)
turn(110)
drive(-15)
drive(10)
driveToToken()
drive(10)
closeArm()
turn(150)
turn1(0,7,14,21)
drive(50)
openArm()
ArmUp()


drive(-40)
turn(190)
drive(5)
drive(30)
turn(95)
driveToToken()
drive(10)
closeArm()
turn(165)
turn1(0,7,14,21)
openArm()
ArmDown()
CloseArm()
drive(70)
openArm()


#drive(-20)
#turn(90)
#turn(-90)

