#include <Servo.h>

Servo ArmRechts;
Servo ArmLinks;
Servo KlappeRechts;
Servo KlappeLinks; 


#define SERIAL_BAUD 115200
#define FW_VER 0

#define ENABLE_L 12  // Enable Pin for the Left Motor
#define ENABLE_R 4  // Enable Pin for the Right Motor

#define PWM_L_BWD 11  // PWM (analog) Pin for the Left Motor; Turn Forward
#define PWM_L_FWD 3 // PWM (analog) Pin for the Left Motor; Turn Backwards

#define PWM_R_FWD 6 // PWM (analog) Pin for the Right Motor; Turn Forward
#define PWM_R_BWD 5 // PWM (analog) Pin for the Right Motor; Turn Backwards

#define PRESCALER_LEFT 1.05
#define PRESCALER_RIGHT 0.93

#define interruptOne 2

  int defaultSpeed = 20;                // default speed
  int turnSpeed = 10;                   // Speed zum drehen
   volatile int encoderCounter = 0;      //zählt die tics des encoders
  int Armdeg = 0;                       // um wie viel gard soll sich der Arm drehen?   
  int pos = 0;                          // die vorherige position des Armes
  int Stand_Arm = 180;

void setup(){
  Serial.begin(SERIAL_BAUD);            // startet serielle Komunikation
  pinMode(ENABLE_L, OUTPUT);
  pinMode(ENABLE_R, OUTPUT);
  pinMode(PWM_L_BWD, OUTPUT);
  pinMode(PWM_L_FWD, OUTPUT);
  pinMode(PWM_R_BWD, OUTPUT);
  pinMode(PWM_R_FWD, OUTPUT);
  pinMode(interruptOne, INPUT);
  
  attachInterrupt(digitalPinToInterrupt(interruptOne), interruptA, CHANGE);
  


  ArmRechts.attach (A1);          // motor liegt auf pin 10
  ArmLinks.attach (A2);           // motor liegt auf pin 9
  //KlappeRechts.attach (6);       // motor liegt auf pin 3 
  //KlappeLinks.attach (3);        // motor liegt auf pin 3*/

  ArmRechts.write(180);
  ArmLinks.write(0);

}

/********************************************************
                    Funktionen
 *******************************************************/

void interruptA() {
    encoderCounter++;
  } 



  


void drive(int dista){
  digitalWrite (ENABLE_L, HIGH);
  digitalWrite (ENABLE_R, HIGH);

  int PWM_L = 0;
  int PWM_R = 0;
  
  if (dista > 0) {
    analogWrite (PWM_L_BWD, 0);
    analogWrite (PWM_R_BWD, 0);
    PWM_L = PWM_L_FWD ;
    PWM_R = PWM_R_FWD ;
  }
  else {
    analogWrite (PWM_R_FWD, 0);
    analogWrite (PWM_L_FWD, 0);
    
    PWM_L = PWM_L_BWD ;
    PWM_R = PWM_R_BWD ;
    
  }

  int ticsToDrive = abs( (float)dista/34.557*644.5);
  encoderCounter = 0;
  while ( abs(encoderCounter) < ticsToDrive){
    if (abs(encoderCounter)< 1000){                                   // die ersten 1000 steps...
      analogWrite (PWM_L, int(50*PRESCALER_LEFT));                     // halbe Geschwindlichkeit
      analogWrite (PWM_R, int(50*PRESCALER_RIGHT));                     // halbe Geschwindlichkeit
    }
    else if  (ticsToDrive - abs(encoderCounter)< 1000){               // die letzten 1000 steps
      analogWrite (PWM_L, int(50*PRESCALER_LEFT));                     // halbe Geschwindlichkeit
      analogWrite (PWM_R, int(50*PRESCALER_RIGHT));                     // halbe Geschwindlichkeit
    }
    else {                                                            // zwischen den ersten und letzten 1000 steps...
      analogWrite (PWM_L, 100*PRESCALER_LEFT);                              // normale Geschwindigkeit
      analogWrite (PWM_R, 100*PRESCALER_RIGHT);                              // normale Geschwindigkeit
    }
    delay (10);
  }

  analogWrite (PWM_L, 0);
  analogWrite (PWM_R, 0);
  delay (100);
  
}

void turn(int deg){                         // deg = winkel
  digitalWrite (ENABLE_L, HIGH);
  digitalWrite (ENABLE_R, HIGH);

  int PWM_L = 0;
  int PWM_R = 0;
  
  if (deg < 0) {                       //links; gegen Urzeigersinn
    analogWrite(PWM_L_BWD, 0 );
    analogWrite (PWM_R_FWD, 0);
    PWM_L = PWM_L_FWD ;
    PWM_R = PWM_R_BWD ;
  }
  else {                                 //rechts; im Urzeigersinn
    analogWrite (PWM_R_BWD, 0);
    analogWrite (PWM_L_FWD, 0);
    
    PWM_L = PWM_L_BWD ;
    PWM_R = PWM_R_FWD ;
    
  }
   float streckeZumMittelpunkt = 22.2;                                          // je größer die Zahl,desto weiter dreht er sich
  int ticsToTurn = abs((float)deg/360.0*2.0*streckeZumMittelpunkt*3.14 /34.557*644.5);   // wie viele tics muss ich fahren?
  encoderCounter = 0;                                                  // setzt die tics auf Null, um von neu an zu zählen
  while ( abs(encoderCounter) < ticsToTurn){                           // während die zu fahrende Stecke noch nicht gefahren wurde ...
    if (abs(encoderCounter)< 700){                      //die ersten 700 Umdrehungen um hälfte langsamer
      analogWrite (PWM_L, int(0,5*turnSpeed));          // halbe Geschwindlichkeit
      analogWrite (PWM_R, int(0,5*turnSpeed));          // halbe Geschwindlichkeit
    }
    else if  (ticsToTurn - abs(encoderCounter)< 700){   // die letzten 700 Umdrehungen um hälfte langsamer
      analogWrite (PWM_L, int(0,5*turnSpeed));          // halbe Geschwindlichkeit
      analogWrite (PWM_R, int(0,5*turnSpeed));          // halbe Geschwindlichkeit
    }
    else {
      analogWrite (PWM_L, turnSpeed);                   //Linker Motor auf Drehgeschwindigkeit  
      analogWrite (PWM_R, turnSpeed);                   //Rechter motor auf Drehgeschwindigkeit 
    }
    delay (10);                                         // 10 milisekunden pause 
  }
 
  analogWrite (PWM_L, 0);                               //Backup auf 0 setzen
  analogWrite (PWM_R, 0);                               //Backup auf 0 setzen
  delay (100);                                          // 0.1 sek pause

  
  
}




void ArmDrehen(int ArmDeg){

    if (Stand_Arm > ArmDeg)
    {
      for (pos = Stand_Arm; pos >= ArmDeg; pos --)
      {
        ArmRechts.write (pos);
        ArmLinks.write (180-pos);
        delay (30);
      }
    }

    if (Stand_Arm < ArmDeg)
    {
      for (pos = Stand_Arm; pos <= ArmDeg; pos ++)
      {
        ArmRechts.write (pos);
        ArmLinks.write (180-pos);
        delay (30);
      }
    }
    Stand_Arm = ArmDeg;

 }

 
 /******************************************
      Dies führt der Roboter aus:
  *******************************************/
void start(){
  /*delay (7000);
  ArmDrehen(1);
  delay(1000);
  ArmDrehen(91); // 180
  delay (1000);
  ArmDrehen(1); //90  
  delay (1000);
  ArmDrehen(175);*/
  
   
  drive(50);
  turn(-90);
  drive(50);
  turn(-90);
  drive(50);
  turn(90);
  drive(-50);
  turn(90);
  
}
/*********************************************/

void loop() {                    
  while (Serial.available()){
      int selected_command = Serial.read();
      switch (selected_command) {
        case 'v':
          Serial.print("SRcustom:");
          Serial.print(FW_VER);
          Serial.print("\n");
          break;
         case 's':
          Serial.print("\n");
          start();
          break;
        default:
          Serial.print("\n");
          break;
      }
  }
}
